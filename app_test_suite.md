# App Test Suite

The *EMPAIA App Test Suite* provides CLI commands to check the syntax of your app's EAD and the requests of your containerized app against the App API. You can also start your app via the EMPAIA Workbench Client and view the results, to get an idea of the user experience.

## Requirements

In order to test your app you need the following:
* Your containerized app (see [Publish - Containerized App Example](publish.md#containerized-app-example))
  * Your app's code
  * Dockerfile
* The EAD for your app (see [Tutorial](tutorial/tutorial.md#) and [Specs - EAD](specs/empaia_app_description.md#))
* [Docker](https://www.docker.com/get-started) installed
* Python 3.8 or above installed

Recommended but not required: [Python venv](https://docs.python.org/3/library/venv.html)

?> We recommend using Linux for this tutorial. Windows 10 and macOS should work, but have not been tested extensively. The code samples below are for Posix (Linux, macOS tested) or Powershell (Windows tested). On Windows use Powershell in [Windows Terminal](https://www.microsoft.com/de-de/p/windows-terminal/9n0dx20hk701), otherwise font rendering might be broken. Problems have been reported with the combination of Windows 10, conda, python > 3.8.0.

## Installation

Download the <a href="resources/empaia_app_test_suite-1.2.0.zip">empaia_app_test_suite-1.2.0.zip</a> and unzip the package:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
unzip empaia_app_test_suite-1.2.0.zip -d eats
cd eats
```
<!-- tab:Windows -->
```powershell
# unzip in explorer to .\eats
cd .\eats
```
<!-- tabs:end -->

Create virtual environment and activate it (recommended but not required):

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
python3 -m venv .venv
source .venv/bin/activate
```
<!-- tab:Windows -->
```powershell
python -m venv .venv
.\.venv\Scripts\Activate.ps1
```
<!-- tabs:end -->

?> On Windows Powershell a PSSecurityException error might occur. In this case, start another Powershell as Admin and run `set-executionpolicy remotesigned`. This command allows you to enable the execution of unsigned scripts. Restart Powershell and try again.

Install the app test suite:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
pip install wheel
pip install ./dist/empaia_app_test_suite-1.2.0-py3-none-any.whl
```
<!-- tab:Windows -->
```powershell
pip install wheel
pip install .\dist\empaia_app_test_suite-1.2.0-py3-none-any.whl
```
<!-- tabs:end -->

## Basic Usage

This section will guide you through using the EATS with the first sample app from the [Tutorial](tutorial/tutorial.md#), i.e. 01_simple_app.
Afterwards you will have executed every step necessary to test your own app and view the results.

Before the actual run, please download a sample WSI from the [openslide website](http://openslide.cs.cmu.edu/download/openslide-testdata/Aperio/CMU-1.svs) and store it at some path **/SOME/PATH/TO/WSIS/CMU-1.svs**.

### Prepare App

Download or clone the [Sample Apps Repository](https://gitlab.com/empaia/integration/sample-apps), known from the tutorial:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
git clone https://gitlab.com/empaia/integration/sample-apps.git
```
<!-- tab:Windows -->
```powershell
git clone https://gitlab.com/empaia/integration/sample-apps.git
```
<!-- tabs:end -->

For all proceeding instructions we assume the following folder structure. Also, interesting sample apps for the Test Suite are shown in the folder structure:

```
eats/
├── .venv/ (if using venv)
├── dist/
├── licenses/
└── sample-apps/
    └── sample_apps/
        └── valid/ 
            ├── others/
                ├── _12_eats_input_templates
                ├── _13_eats_nested_large_collections
            └── tutorial/
                ├── _01_simple_app/
                ├── _02_collections_and_references/
                ├── _03_annotation_results/
                ├── _04_output_references_output
                ├── _05_classes
                ├── _06_large_collections
                └── _07_configuration/
```

Next we build the docker image for the first example app of the tutorial:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
docker build -t app01 ./sample-apps/sample_apps/valid/tutorial/_01_simple_app/
```
<!-- tab:Windows -->
```powershell
docker build -t app01 .\sample-apps\sample_apps\valid\tutorial\_01_simple_app\
```
<!-- tabs:end -->

### Help Command

For information about the CLI arguments execute:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats --help
```
<!-- tab:Windows -->
```powershell
eats --help
```
<!-- tabs:end -->

For more information on each sub command, e.g.:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats jobs --help
eats jobs register --help
```
<!-- tab:Windows -->
```powershell
eats jobs --help
eats jobs register --help
```
<!-- tabs:end -->

### Start services

First we need to prepare a file, that tells the eats services where to find our WSIs, i.e. the WSI we just downloaded to **/SOME/PATH/TO/WSIS/CMU-1.svs**:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
code ./wsi-mount-points.json
```
<!-- tab:Windows -->
```powershell
code .\wsi-mount-points.json
```
<!-- tabs:end -->

?> The `code` command starts Visual Studio Code to edit the file, but you can use any other text editor instead.

<!-- tabs:start -->
<!-- tab:Posix -->
```JSON
{
    "/SOME/PATH/TO/WSIS": "/data/",
    "/SOME/OTHER/PATH/TO/WSIS": "/data2/"
}
```
<!-- tab:Windows -->
```powershell
{
    "C:\\SOME\\PATH\\TO\\WSIS": "/data/",
    "C:\\SOME\\OTHER\\PATH\\TO\\WSIS": "/data2/"
}
```
<!-- tabs:end -->

You can specify as many paths as you like, illustrated by the second line. For this walkthrough, we only need the first mount point though.

Let us take a look at the inputs for our app:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
cd ./sample-apps/sample_apps/valid/tutorial/_01_simple_app/
code ./eats_inputs/my_wsi.json
cd -
```
<!-- tab:Windows -->
```powershell
pushd .\sample-apps\sample_apps\valid\tutorial\_01_simple_app\
code .\eats_inputs\my_wsi.json
popd
```
<!-- tabs:end -->

```JSON
{
    "type": "wsi",
    "path": "/data/CMU-1.svs",
    "id": "37bd11b8-3995-4377-bf57-e718e797d515"
}
```

No need to edit anything here, just note the `"path"`, that contains our mount point. If more WSIs reside in **/SOME/PATH/TO/WSIS** or a subdirectory, they can be specified as well.

Start the services with:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats services up ./wsi-mount-points.json
```
<!-- tab:Windows -->
```powershell
eats services up .\wsi-mount-points.json
```
<!-- tabs:end -->

?> The very first run of the `eats` requires some minutes, as quite a few background services need to be build as docker images.

?> You can activate isyntax image file support by running `services up` with flag `--isyntax-sdk=<path/to/philips-pathologysdk-2.0-ubuntu18_04_py36_research.zip>` once. To get the SDK you have to register and download it from the Philips Pathology SDK Site (Note: Make sure to download the version for Ubuntu 18.04 and Python 3.6.9).

### Register App

First we need to register our app at our locally running Solution-Store-Service. Providing the `docker-image-name`, i.e. `app01`, the path to the `ead.json`. If your app requires configuration parameters, append ` --config-file <path/to/configuration.json>`.

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
cd ./sample-apps/sample_apps/valid/tutorial/_01_simple_app/
eats apps register ead.json app01 > app.env
```
<!-- tab:Windows -->
```powershell
cd .\sample-apps\sample_apps\valid\tutorial\_01_simple_app\
eats apps register ead.json app01 > app.env
```
<!-- tabs:end -->

The command will output an `APP_ID`, which we can store as variable for later use with:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
export $(xargs <app.env)
echo $APP_ID
```
<!-- tab:Windows -->
```powershell
# Note: If variable already assigned, delete with:
# Remove-Variable APP_ID
Get-Content app.env | Foreach-Object{
   $var = $_.Split('=')
   New-Variable -Name $var[0] -Value $var[1]
}
$APP_ID
```
<!-- tabs:end -->

To view all registered apps use:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats apps list
```
<!-- tab:Windows -->
```powershell
eats apps list
```
<!-- tabs:end -->

### Register and Run Job

Register a new job using the `APP_ID` and the folder containing the inputs and save the output to `job.env`. The output includes the environment variables for your app (`EMPAIA_JOB_ID`, `EMPAIA_TOKEN`, `EMPAIA_APP_API`):

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats jobs register $APP_ID ./eats_inputs > ./job.env
```
<!-- tab:Windows -->
```powershell
eats jobs register $APP_ID .\eats_inputs > .\job.env
```
<!-- tabs:end -->

Run the app, providing the file including the environment variables `job.env`:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats jobs run ./job.env
```
<!-- tab:Windows -->
```powershell
eats jobs run .\job.env
```
<!-- tabs:end -->

### Job Status

The job ID can be retrieved from the `job.env` file to be used in other commands.

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
export $(xargs <job.env)
echo $EMPAIA_JOB_ID
```
<!-- tab:Windows -->
```powershell
# Note: If variables already assigned, delete with:
# Remove-Variable EMPAIA_JOB_ID
# Remove-Variable EMPAIA_TOKEN
# Remove-Variable EMPAIA_APP_API
Get-Content job.env | Foreach-Object{
   $var = $_.Split('=')
   New-Variable -Name $var[0] -Value $var[1]
}
$EMPAIA_JOB_ID
```
<!-- tabs:end -->

Regularly check the job's status until it is `COMPLETED`.

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats jobs status ${EMPAIA_JOB_ID}
```
<!-- tab:Windows -->
```powershell
eats jobs status ${EMPAIA_JOB_ID}
```
<!-- tabs:end -->

### Results - JSON

To export the output primitives (as well as all other outputs):

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats jobs export ${EMPAIA_JOB_ID} ./job-outputs
```
<!-- tab:Windows -->
```powershell
eats jobs export ${EMPAIA_JOB_ID} ./job-outputs
```
<!-- tabs:end -->

### Results - Workbench Client

Open http://localhost:8080 in a Browser to review job results using the EMPAIA Workbench Client. The following screenshots show the results of app **04_output_references_output**.

For simplicity, all slides are added to a static single case. Navigate to:

1) **Cases** and select the case:

<img src="images/app_test_suite/wbc_case.png"></img>

2) **Examinations** and select the OPEN examination:

<img src="images/app_test_suite/wbc_examination.png"></img>

3) **Apps** and select your app:

<img src="images/app_test_suite/wbc_app.png"></img>

4) **Jobs** and select the latest job:

<img src="images/app_test_suite/wbc_job.png"></img>

5a) Explore the tree view. E.g. click **\[Root\]** to see primitive outputs of your app with no reference or a reference to a collection (bottom right) (none in this job).

5b) Click the **\[WSI\]...** to view the slide and see primitive outputs referencing the slide (bottom right) (none in this job).

5c) Click **\[Rectangle\]...** to see primitive outputs referencing the item (bottom right) (none in this job). Also note, that depending on your position in the tree view, the annotations list on the left changes according to the references, i.e. (collections of) annotations referencing the **Rectangle**:

<img src="images/app_test_suite/wbc_job_results_collection.png"></img>

5d) Click **\[Point\]...** to see primitive outputs referencing the item (bottom right):

<img src="images/app_test_suite/wbc_job_results_primitive.png"></img>


?> The annotations **line** and **arrows** are not yet supported. Viewing a job containing one of these data types will result in no annotations loading.

?> IDs or whole objects in JSON-format can easily be copied to the clipboard in the Workbench Client. Therefore, the user needs to hold the key `i` (for ID) or `j` (for a string in JSON-notation) and click on a desired item in one of the UI-panels.

## Advanced Usage

This section contains information on further features of the EATS, e.g. executing your app from within the Workbench Client, just like a user would do it, or features to help debugging your app.

### Enable GPU support

In order to run a Docker Image with GPU support, (a) the host has to have appropriate GPU capabilities, and (b) the Docker containers have to be started with specific parameters in order to request that GPU support. Requesting GPU support when none is available results in an error.

Thus, by default, if the parameter is not provided, the Job Execution Service within the EATS does not make use of GPUs. To activate GPU support within the EATS and make it request GPU support for each started app, you have to specify the GPU driver when starting the EATS, using the optional `--gpu-driver` parameter.

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats services up --gpu-driver nvidia ./wsi-mount-points.json
```
<!-- tab:Windows -->
```powershell
eats services up --gpu-driver nvidia ./wsi-mount-points.json
```
<!-- tabs:end -->

Here, `--gpu-driver nvidia` is used to enable CUDA support. The values are not defined and evaluated by the EATS but passed directly to Docker when starting the app images. Please refer to the Docker documentation for other possible values. If the parameter is not provided, GPU support is disabled.

At the moment, the EMPAIA compute environment is set up to support CUDA, i.e. `nvidia` GPUs; other GPU drivers may work, too, if they are installed on the host system, but are not yet officially supported. Note, however, that not each compute environment may have GPU support. To ensure maximum compatibility, it is recommended to implement a fallback such that the app works both, with and without GPU support. In case the app can not work without GPUs, it should check for the availability of GPU support and exit with an appropriate status code and message if none is available.


### Executing Your App Within Workbench Client

All slides, that have been used as job-inputs (via `eats jobs register`), will be available in the Workbench Client in one case.
If you want to add further slides, without registering jobs via CLI, you can use the following command:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats slides register <path-to-wsi-input.json>
```
<!-- tab:Windows -->
```powershell
eats slides register <path-to-wsi-input.json>
```
<!-- tabs:end -->

Also, all apps, that have been registered (via `eats apps register`), will be available to be executed as new jobs within the Workbench Client under **Apps**. For this purpose, select your app and in the following **Jobs** tab and click on **New** to open a wizard to select your inputs:

<img src="images/app_test_suite/wbc_new_job.png"></img>


?> Currently the wizard only supports EADs with exactly 1 WSI input and 1 Rectangle / Circle / Polygon input, or a collection of these annotations.


### Debugging

The job ID is used as the container name. It can be used to retrieve docker logs:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
docker logs ${EMPAIA_JOB_ID}
```
<!-- tab:Windows -->
```powershell
docker logs ${EMPAIA_JOB_ID}
```
<!-- tabs:end -->

If a job is taking too long or is stuck, the job can be aborted:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats jobs abort ${EMPAIA_JOB_ID}
```
<!-- tab:Windows -->
```powershell
eats jobs abort ${EMPAIA_JOB_ID}
```
<!-- tabs:end -->

To inspect backend service logs the `docker logs` command can be used directly. The names of all service containers can be retrieved using the `eats services containers` command. For example, we can retrieve the logs of the `app-service` to see the requests our app has sent to the API:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats services list  # print list of service names
docker logs app-service
```
<!-- tab:Windows -->
```powershell
eats services list  # print list of service names
docker logs app-service
```
<!-- tabs:end -->

### Stop Services and Delete Jobs

It is possible to register and run multiple jobs without restarting backend services. The services can be stopped, if they are not needed anymore.

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats services down
```
<!-- tab:Windows -->
```powershell
eats services down
```
<!-- tabs:end -->

To erase all data (registered WSIs, register apps, jobs, etc.) use the `-v` switch:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats services down -v
```
<!-- tab:Windows -->
```powershell
eats services down -v
```
<!-- tabs:end -->

### References

The `id` property in input files for an app does not necessarily have to be present. But when an input is used as reference for another input, the `id` **must** be set (uuid4). For example, take a look at `my_wsi.json` and `my_rectangle.json` inside `sample-apps/sample_apps/valid/tutorial/_01_simple_app/eats_inputs`:

`my_wsi.json` (see `id`):

```JSON
{
    "type": "wsi",
    "path": "/data/CMU-1.svs",
    "id": "37bd11b8-3995-4377-bf57-e718e797d515"
}
```

`my_rectangle.json` (see `reference_id`)
```JSON
{
    "type": "rectangle",
    "upper_left": [
        1000,
        2000
    ],
    "width": 300,
    "height": 500,
    "reference_id": "37bd11b8-3995-4377-bf57-e718e797d515",
    "npp_created": 499,
    "npp_viewing": [1, 499123]
}
```

!> When defining an input collection, either all items and the collection itself must have an `id` or none at all.

!> When modifying an input JSON-file with `id` property set, you either have to delete your previously stored data (`docker volume rm $(eats services volumes)`), or assign a new `id` if you want to keep your previous jobs including all data.

### Classes as Input

It might happen that some input annotations in your EAD have a `classes` constraint, but the classes themselves are not an input parameter. In this case, the classes still have to be defined as input JSON-files. For reference, see the property `inputs.my_rectangles.items.classes` in `sample-apps/sample_apps/valid/tutorial/_05_classes/ead.json`:

```JSON
{
    "$schema": "https://developer.empaia.org/schema/ead-app-schema-draft-3.json",
    "name": "Tutorial App 05",
    "name_short": "TA05",
    "namespace": "org.empaia.vendor_name.tutorial_app_05.v1",
    "description": "Human readable description",
    "classes": {
        "tumor": {
            "name": "Tumor"
        },
        "non_tumor": {
            "name": "Non tumor"
        }
    },
    "inputs": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangles": {
            "type": "collection",
            "reference": "inputs.my_wsi",
            "items": {
                "type": "rectangle",
                "reference": "inputs.my_wsi",
                "classes": [
                    "org.empaia.global.v1.classes.roi"
                ]
            }
        }
    },
    "outputs": {
        "my_cells": {
            "type": "collection",
            "items": {
                "type": "collection",
                "reference": "inputs.my_rectangles.items",
                "items": {
                    "type": "point",
                    "reference": "inputs.my_wsi"
                }
            }
        },
       "my_cell_classes": {
            "type": "collection",
            "items": {
                "type": "collection",
                "items": {
                    "type": "class",
                    "reference": "outputs.my_cells.items.items"
                }
            }
        }
    }
}
```

The corresponding input directory `/sample-apps/sample_apps/valid/tutorial/_05_classes/eats_inputs` contains the files `my_wsi.json`, `my_rectangles.json`, but also a file `rois.json`:

```JSON
{
    "item_type": "class",
    "items": [
        {
            "value": "org.empaia.global.v1.classes.roi",
            "reference_id": "73621cf4-1c2d-44fd-99fc-4937afda8519"
        },
        {
            "value": "org.empaia.global.v1.classes.roi",
            "reference_id": "0fb368f6-9345-405a-83af-ab4d38141cd5"
        },
        {
            "value": "org.empaia.global.v1.classes.roi",
            "reference_id": "2026af14-9108-49dc-992d-6eb4315f789e"
        }
    ]
}
```

### Configuration Parameter

If your app contains configuration parameters, have a look at sample app `sample-apps/sample_apps/valid/tutorial/_07_configuration/ead.json` and `sample-apps/sample_apps/valid/tutorial/_07_configuration/eats_inputs/configuration/configuration.json`. Registering the job, add the argument `--config-file`:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
cd ../_07_configuration
docker build -t app07 .
eats apps register app07 ead.json --config-file ./eats_inputs/configuration/configuration.json
```
<!-- tab:Windows -->
```powershell
cd ..\_07_configuration
docker build -t app07 .
eats apps register ead.json app07 --config-file .\eats_inputs\configuration\configuration.json
```
<!-- tabs:end -->

?> Note: The configuration JSON-file **must not** reside directly in the inputs' directory. At least a subdirectory should be used.

### Adapt to Your App

In order to write the required `.json` files to specify the input of your app, please refer to the content of the sample_apps `sample-apps/sample_apps/valid/` and their corresponding input files.

An example app with all different kinds of input variables, can be found under `sample-apps/sample_apps/valid/others/_12_eats_input_templates`.


### Executing Your App without Job Execution Service

The command `eats jobs run $EMPAIA_JOB_ID <job.env-file>` sends a request to the Job Execution Service to start your containerized app. Debugging your app this way can be tedious and time-consuming. To speed up the debugging process, you can also start your app natively:


1. Start services as usual.

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats services up ./wsi-mount-points.json
```
<!-- tab:Windows -->
```powershell
eats services up .\wsi-mount-points.json
```
<!-- tabs:end -->

2. Register app as usual, receiving the `APP_ID`

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats apps register <path-to-ead.json> <docker-image-name> --config-file <path-to-configuration.json> > app.env
export $(xargs <job.env)
echo $APP_ID
```
<!-- tab:Windows -->
```powershell
# Note: If variable already assigned, delete with:
# Remove-Variable APP_ID
Get-Content app.env | Foreach-Object{
   $var = $_.Split('=')
   New-Variable -Name $var[0] -Value $var[1]
}
$APP_ID
```
<!-- tabs:end -->

3. Register job and export to `job.env`, e.g.

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats jobs register $APP_ID <eats-inputs-folder> > job.env
export $(xargs <job.env)
echo $EMPAIA_JOB_ID
```
<!-- tab:Windows -->
```powershell
# Note: If variables already assigned, delete with:
# Remove-Variable EMPAIA_JOB_ID
# Remove-Variable EMPAIA_TOKEN
# Remove-Variable EMPAIA_APP_API
Get-Content job.env | Foreach-Object{
   $var = $_.Split('=')
   New-Variable -Name $var[0] -Value $var[1]
}
$EMPAIA_JOB_ID
```
<!-- tabs:end -->

4. Overwrite the `EMPAIA_APP_API` value with `localhost` and the exposed port of the app-service container (`8082` by default):

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
export EMPAIA_APP_API=http://localhost:8082
```
<!-- tab:Windows -->
```powershell
$EMPAIA_APP_API=http://localhost:8082
```
<!-- tabs:end -->

5. Set job to status `RUNNING` (without actually running it):

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
eats jobs set-running $EMPAIA_JOB_ID
```
<!-- tab:Windows -->
```powershell
eats jobs set-running $EMPAIA_JOB_ID
```
<!-- tabs:end -->

6. Run your app, e.g.:

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
python3 <path-to-app.py>
```
<!-- tab:Windows -->
```powershell
python3 <path-to-app.py>
```
<!-- tabs:end -->

7. Make changes to your app.py and **GO TO 3.**

### Configuring Docker behind a proxy

In case you are using the EATS behind a proxy, make sure your docker proxy-configuration is set correctly.

1. Navigate to `.docker` in your user directory

<!-- tabs:start -->
<!-- tab:Posix -->
```bash
cd ~/.docker
```

<!-- tab:Windows -->
```powershell
cd C:\Users\Username\.docker\machine\default
```
<!-- tabs:end -->

2. Exclude all used EMPAIA services in your `config.json` as defined below:

```JSON
{
    "proxies": {
        "default": {
            "httpProxy": "http://proxy.example.de:8080",
            "httpsProxy": "http://proxy.example.de:8080",
            "noProxy": "
                *.example.de,
                127.0.0.1,
                localhost,
                annotation-service,
                app-service,
                clinical-data-service,
                ead-validation-service,
                eats-postgres-db,
                eats-mongo-db,
                examination-service,
                job-execution-service,
                job-service,
                medical-data-service,
                solution-store-service-mock,
                storage-mapper-service,
                vault-service-mock,
                workbench-client,
                workbench-daemon,
                workbench-service,
                wsi-service"
        }
    }
}
```