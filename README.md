**Version: Draft-3**

# App Developer Documentation

Welcome to the EMPAIA App Developer Docs. The following guides provide you with the required information to create, describe and publish your data processing applications (apps) for histology, in a way that they are compatible with the EMPAIA ecosystem.

The EMPAIA project is currently in early stage of developing a Minimum Viable Product (MVP). Therefore changes to this document are likely. For example, beyond the MVP scope, we are planning to introduce solutions, which may consist of more than one app.

To gain a better understanding of the infrastrucutre provided by EMPAIA, please take a look at the documents linked under [References](#references). Afterwards we suggest to read through the [Tutorial](tutorial/tutorial.md#), which will introduce you to the EMPAIA App Description (EAD) format and the EMPAIA App API. For details, please refer to [Specs](specs/specs.md#).

App developers can check the API compliance of their apps using the [EMPAIA App Test Suite (EATS)](app_test_suite.md#).

## Contact

* If: you have found an error in this documentation or would like to request an EAD feature please use the [Docs Issue Tracker](https://gitlab.com/empaia/documentation/app-developer-docs/-/issues)
* Elif: you have found a software bug or would like to request an EATS feature please use the [EATS Issue Tracker](https://gitlab.com/empaia/integration/empaia-app-test-suite/-/issues)
* Else: contact us directly via [dev-support@empaia.org](mailto:dev-support@empaia.org)

## References

**Open Source Repositories**

* [App Developer Docs](https://gitlab.com/empaia/documentation/app-developer-docs)
* [EMPAIA App Test Suite](https://gitlab.com/empaia/integration/empaia-app-test-suite)
* [EMPAIA Services](https://gitlab.com/empaia/services)

**App API**

Proprietary AI apps can be easily integrated into the EMPAIA Platform by implementing the lightweight EMPAIA App API. 

<img src="images/app_api.png"></img>

**EMPAIA Components**

Detailed overview of the EMPAIA platform and its components.

<img src="images/detailed_apis.png"></img>

**Presentation Slides**

<a href="resources/EMPAIA_Solutions.pdf" download>EMPAIA_Solutions.pdf</a>

## Change Log

### Pinned

* The slides under [References](#references) mention two use cases:
  1. User applies a solution.
  2. Solution is automatically applied for preprocessing of new slides.
* The code examples in this document always refer to (1. User applies a solution).
* Draft-3 does not yet support solutions, but only standalone apps.
  * In later versions apps will be the building blocks of solutions.
* App in the context of this document always refers to the concept of a containerized app.

### Draft-3 Update 2021-11-22

* EATS 1.2.0 released (<a href="resources/empaia_app_test_suite-1.2.0.zip">download</a>).
  * Workbench Client is now included as Web UI to replace the App Test Suite Web Client
  * Apps can now also be started via the Workbench Client
  * Viewing job results now includes primitives
  * Annotations are now structured in the results view (according to references)
  * New CLI commands:
    * eats apps register
    * eats apps list
    * eats slides register
    * eats jobs set-running
  * Changed CLI commands (e.g. parameters):
    * eats jobs register
    * eats jobs run
  * Removed CLI commands:
    * eats jobs id (can be achieved with native shell commands)
* EATS instructions have been updated accordingly [App Test Suite](app_test_suite.md#)
* Renamed App Service API to App API, Medical Data Service API to Medical Data API and Workbench Service API to Workbench API
* Fixed bug in [sample-app-05](tutorial/classes.md#), where RoI classes values where specified as `org.empaia.global.v1.roi` instead of `org.empaia.global.v1.classes.roi`.


### Draft-3 Update 2021-09-06

* Documented HTTP/S proxy usage requirements for apps using [external service providers](specs/external_service_providers.md#)

### Draft-3 Update 2021-07-14

* Published <a href="resources/empaia_app_test_suite-1.1.5.zip">empaia_app_test_suite-1.1.5.zip</a>.
  * EATS is now [open source](https://gitlab.com/empaia/integration/empaia-app-test-suite)
  * enabled token validation in app-service to detect if apps set auth header correctly
  * updated wsi-service to fix concurrency bug, that potentially occurred when closing openslide objects

### Draft-3 Update 2021-06-29

* Published <a href="resources/empaia_app_test_suite-1.1.4.zip">empaia_app_test_suite-1.1.4.zip</a>.
  * added missing validation for optional reference_id and reference_type fields, such that either both or none must be present

### Draft-3 Update 2021-06-28

* Fixed missing `reference_type` in output collection of app code of [tutorial output references output chapter](tutorial/output_references_output.md#).
* Fixed wrong EAD of [tutorial large collections chapter](tutorial/large_collections.md#).
  * Changes of 2021-05-10 were not reflected in the EAD.
* Sample Apps in (e.g in [App Test Suite](app_test_suite.md#)) are now linked via public gitlab.com repository (instead of zip archive):
  * [Sample Apps Repository](https://gitlab.com/empaia/integration/sample-apps).

### Draft-3 Update 2021-06-24

* Published <a href="resources/empaia_app_test_suite-1.1.3.zip">empaia_app_test_suite-1.1.3.zip</a>.
  * fixing bug in web-client, that prevents certain slides of being viewed.
  * updated annotation colors for better contrast.
* Published <a href="resources/empaia_app_test_suite-1.1.2.zip">empaia_app_test_suite-1.1.2.zip</a>.
  * performance improvements.
  * usability improvements.
  * minor bug fixes.

### Draft-3 Update 2021-06-01

* Published <a href="resources/empaia_app_test_suite-1.1.1.zip">empaia_app_test_suite-1.1.1.zip</a>.
  * fixed an issue preventing changes of 1.1.0 being effective.

### Draft-3 Update 2021-05-28

* Published <a href="resources/empaia_app_test_suite-1.1.0.zip">empaia_app_test_suite-1.1.0.zip</a>.
  * contains fix to preserve input order of data elements returned from POST request in derived-annotation-data-service.

### Draft-3 Update 2021-05-25

* Documented [purpose of EAD fields](specs/empaia_app_description.md#?id=other-fields) `name`, `name_short`, and `description`.
* Documented [Annotation Rendering](specs/annotation_rendering.md#).
* Removed unnecessary reference from input collection to input WSI in tutorial apps, because it does nothing useful and is misleading.

### Draft-3 2021-05-10

* Updated [App Test Suite](app_test_suite.md#) for new version empaia_app_test_suite-1.0.0
  * CLI changed.
  * Requirements for input parameter JSON-files changed.
  * Web Slide and Annotation Viewer included.
* Added page [Purpose of References](specs/references.md#)
* Updates EAD:
  * `reference` is now a required property of annotations (must reference a WSI).
  * `reference` is now a required property of classes (must reference an annotation).
  * Removed mapping of classes.
  * Updated Tutorial accordingly.
* `reference_type` now must be set by the app. Tutorial and Specs have been updated accordingly.
* Refactored [tutorial large collections chapter](tutorial/large_collections.md#):
  * Removed mapping of classes.
  * Focuses now on getting and posting large collections in batches.

### Draft-3 2021-03-29

* Removed `optional` and `default` properties for all input and output data type in EAD.
* Added WSI data usage guide and list of supported WSI formats [Specs/WSI-Data](specs/wsi_data.md#).
* Removed downsampled levels of WSI-Data [Specs/Empaia-App-Description](specs/empaia_app_description.md#?id=references).
* Added references for all tutorials for collections `inputs.my_rectangles` to `my_wsi`.
* Added `name_short` to EAD.
* Added string and property validation checks to EAD JSON Schema draft-3.
* EAD now hosted at [https://developer.empaia.org/src/ead-app-schema-draft-3.json](https://developer.empaia.org/src/ead-app-schema-draft-3.json)
* Added hint about problems with the test suite for the combination of Windows10, conda, python>3.8.0 [EMPAIA-Test-Suite](test_suite.md#).
* Updated reference table [Specs/Empaia-App-Description](specs/empaia_app_description.md#?id=references), the following references are no longer possible:
  * collection -> collection
  * collection -> primitive
  * primitive -> primitive
* Added new [tutorial chapter](tutorial/configuration.md#) on confiuration parameters for EAD and App API.
* Added Vault Service to [Glossary](glossary.md#?id=vault-service).
* Added section to [Specs/Empaia-App-Description](specs/empaia_app_description.md#?id=configuration) for configuration parametes.
* Updated [Specs/App-API](specs/app_service_api.md#) for configuration parameter endpoint.
* Updated all locations with any Annotation:
  * Removed `resolution` (optional) attribute.
  * Added `npp_created` (mandatory) attribute.
  * Added `npp_viewing` (optional) attribute.
  * Added explanation to `npp_created` and `npp_viewing` in [Specs/Empaia-App-Description](specs/empaia_app_description.md#?id=annotation-data-types).
  * Added explanation to `npp_created` and `npp_viewing` in [Tutorial/Empaia-App-Description](tutorial/annotation_results.md#).
  * Updated [Specs/App-API](specs/app_service_api.md#).
  * Updated [EMPAIA-Test-Suite](test_suite.md#).

### Draft-2 2020-12-01

* Replaced App API version endpoint `.../v1/...` with `.../v0/...` (Tutorial pages, tutorial.zip, Publish page, containerized_app_example.zip, Specs pages).
* Added detailed picture of App API (Introduction page).
* Replaced EMPAIA components diagram with a more detailed one (Introduction page).
* Fixed bug in python code `large_collections` (Tutorial pages, tutorial.zip).
* Added `type` property when sending primitive or annotation outputs to the App API (Tutorial pages, tutorial.zip, Publish page, containerized_app_example.zip)
* Added content to [EMPAIA-Test-Suite](app_test_suite.md#).
* Fixed bug loading image with pillow in tutorial code, missing `BytesIO` (Tutorial pages, tutorial.zip, Publish page, containerized_app_example.zip)
* Added further information on how to build and test docker container under [Publish](publish.md#).
* Updated arrow annotation model (replaced `start_point`, `end_point` with `head`, `tail` keywords).
* Added information about the annotation coordinate system in the [Annotation Data Types](specs/empaia_app_description.md#?id=annotation-data-types).
* Added information about the [WSI API](specs/empaia_app_description.md#?id=wsi-data-type).
* Updated Redoc App-API.
* Fixed bugs in tutorial code.
* Fixed broken links.
