# External Service Providers

If an app transmits data to an external service provider, the app must implement the following specification.

?> These rules are currently not enforced by the EMPAIA App Test Suite, due to technical limitations.

1. If the app sends data to an external API, the property `data_transmission_to_external_service_provider` must be included in the EAD and set to `true`.
2. Only encrpyted HTTPS connections on standard port 443 are allowed. Unencrypted HTTP connections on standard port 80 might be possible in a temporary test setting.
3. The app must use the HTTP/S proxy settings defined by `http_proxy`, `https_proxy` and `no_proxy` environment variables.
    * For detailed information about these variables refer to the [gitlab blog](https://about.gitlab.com/blog/2021/01/27/we-need-to-talk-no-proxy/).


## Proxy Implementation in Python Apps

If the app uses the `requests` package, the HTTP/S proxy is configured automatically. Other HTTP/S clients (e.g. `aiohttp`) require an explicite proxy setting, that can be achieved using the following recipe.

```python
from urllib.parse import urlparse
from urllib.request import getproxies_environment, proxy_bypass_environment

def proxy(url):
    parsed_url = urlparse(url)
    host = parsed_url.netloc.split(":")[0]
    proxies = getproxies_environment()
    if parsed_url.scheme not in proxies:
        return None
    if proxy_bypass_environment(host=host, proxies=proxies):
        return None
    return proxies[parsed_url.scheme]
```
