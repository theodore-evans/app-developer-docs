# Specs

If you have not yet done so, we suggest to first read through the [Tutorial](tutorial/tutorial.md#) to get familiar with the EMPAIA-App-Description (EAD) and the App API. The tutorial might not cover everything needed to make your app EMPAIA compliant. If this is the case refer to this chapter.
