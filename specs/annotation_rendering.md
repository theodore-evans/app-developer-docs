# Annotation Rendering

In order to reduce the number of annotations that have to be queried and rendered by the slide-viewer contained in the EMPAIA App Test Suite Web-Client, the query sets a filter option to only match annotations, that are located on the WSI level currently viewed.

The formula used by the viewer takes into account the current resolution value `c` (unit npp: nanometers per pixel) to calculate a lower boundary `a` and an upper boundary `b`.

```python
a = (c/2 + c)/2
b = (c*2 + c)/2
npp_viewing = [a, b]
```

An app, that creates annotations as output, has to define an `npp_created` field. If the `npp_created` value lies between the upper and lower boundary, it is matched for the query and rendered in the viewer.

If the app defines the optional `npp_viewing` field for an annotation, it can set a resolution range. The annotation will match the slide-viewer's query, if the `npp_viewing` range of the query and the `npp_viewing` range of the annotation overlap. The `npp_created` value will be ignored.

For every annotation, that is located on a higher resolution than requested by the query, a centroid (x, y coordinates) is returned. The annotation centroids are used to visualize these annotations in a cluster. Therefore the user knows where to find more annotations when zooming in.
