# WSI Data

This chapter takes a closer look at the WSI endpoints by example:

```python
endpoint_1 = f"{APP_API}/v0/{JOB_ID}/inputs/my_wsi"
endpoint_2 = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{size_x}/{size_y}"
endpoint_3 = f"{APP_API}/v0/{JOB_ID}/tiles/{wsi_id}/level/{level}/position/{pos_x}/{pos_y}"
```

We assume the EAD and the app from the first tutorial section, i.e. [01_Simple_App](tutorial/simple_app.md#), hence, the identifier `my_wsi`.

## Example Usage

We assume `my_wsi` to be the Aperio WSI hosted by OpenSlide at:

http://openslide.cs.cmu.edu/download/openslide-testdata/Aperio/CMU-1.svs

Calling `endpoint_1` in our app we get the following data:

```python
from pprint import pprint

# (...) for complete code, see tutorial

url = f"{APP_API}/v0/{JOB_ID}/inputs/my_wsi"
r = requests.get(url, headers=HEADERS)
r.raise_for_status()
my_wsi = r.json()
pprint(my_wsi)
```

```JSON
{
    "id": "f985bd96-0f96-41bb-a3ce-af9f04d460f8",
    "channels": [
        {
            "id": 0,
            "name": "Red",
            "color_int": 16711680
        },
        {
            "id": 1,
            "name": "Green",
            "color_int": 65280
        },
        {
            "id": 2,
            "name": "Blue",
            "color_int": 255
        }
    ],
    "channel_depth": 8,
        "extent": {
        "x": 46000,
        "y": 32914,
        "z": 1
    },
    "num_levels": 3,
    "pixel_size_nm": {
        "x": 499,
        "y": 499,
        "z": null
    },
    "tile_extent": {
        "x": 256,
        "y": 256,
        "z": 1
    },
    "levels": [
        {
            "extent": {
                "x": 46000,
                "y": 32914,
                "z": 1
            },
            "downsample_factor": 1
        },
        {
            "extent": {
                "x": 11500,
                "y": 8228,
                "z": 1
            },
            "downsample_factor": 4.000121536217793
        },
        {
            "extent": {
                "x": 2875,
                "y": 2057,
                "z": 1
            },
            "downsample_factor": 16.00048614487117
        }
    ]
}
```

### Valid Tile and Region Requests

This information can then be used to request tiles and regions. Valid request would be:

```python
# regions endpoint
wsi_id = my_wsi["id"] # see endpoint_1 above
region_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/0/start/0/0/size/1024/1024"
r = requests.get(tile_url, headers=HEADERS)
img = Image.open(BytesIO(r.content))
```

```python
# tiles endpoint
tile_url = f"{APP_API}/v0/{JOB_ID}/tiles/{wsi_id}/level/0/position/0/0"
```

The following is also valid, but half of the resulting image only consists of white pixels as the region exceeds the WSI's x coordinates `0-2875` of level `2`:

```python
region_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/0/start/2300/0/size/1024/1024"
```

### Invalid Tile and Region Requests

The following endpoints are invalid, as level `3` is not provided by the WSI.

```python
# regions endpoint
region_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/3/start/0/0/size/1024/1024"
```

```python
# tiles endpoint
tile_url = f"{APP_API}/v0/{JOB_ID}/tiles/{wsi_id}/level/3/position/0/0"
```

?> OpenSlide and other libraries might support more levels. But these are artificially downsampled and not natively provided by the WSI, which complicates clinical usage.


## Example Fluorescence WSI

At time of writing fluorsecence images are only supported throught the OME-TIFF image format. Proprietary formats like `czi` need to be converted to OME-TIFF first (see article on image.sc: https://forum.image.sc/t/converting-whole-slide-images-to-ome-tiff-a-new-workflow/32110).

A sample fluorescence WSI in OME-TIFF format can be found here:

https://downloads.openmicroscopy.org/images/OME-TIFF/2016-06/sub-resolutions/Fluorescence/

If `my_wsi` is a fluorescence WSI, the JSON response of `endpoint_1` will be similar to the following:

```JSON
{
    "id": "3afa68e6-7a88-4b80-aeb9-636d64f33323",
    "channels": [
        {
            "id": 0,
            "name": "AF555",
            "color_int": -8519680
        },
        {
            "id": 1,
            "name": "AF488",
            "color_int": 16724736
        },
        {
            "id": 2,
            "name": "DAPI",
            "color_int": 10551040
        }
    ],
    "channel_depth": 16,
    "extent": {
        "x": 11260,
        "y": 22300,
        "z": 1
    },
    "num_levels": 3,
    "pixel_size_nm": {
        "x": 325,
        "y": 325,
        "z": null
    },
    "tile_extent": {
        "x": 256,
        "y": 256,
        "z": 1
    },
    "levels": [
        {
            "extent": {
                "x": 11260,
                "y": 22300,
                "z": 1
            },
            "downsample_factor": 1
        },
        {
            "extent": {
                "x": 2815,
                "y": 5575,
                "z": 1
            },
            "downsample_factor": 4
        },
        {
            "extent": {
                "x": 703,
                "y": 1393,
                "z": 1
            },
            "downsample_factor": 16.017069701280228
        }
    ]
}
```

Especially for fluorescence images, the parameters `channels` and `channel_depth` are essential. While the channel depth represents the size of each pixel of the image in bits, the channel parameter specifies an array with information about each color channel. This channel parameters consists of an id, a descriptive name and a color integer, that can be interpreted as a 32-bit RGBA-value (e.g. in python by shifting and masking:

```
rgb_value = [(color_int >> 24) & 0xFF, (color_int >> 16) & 0xFF, (color_int >> 8) & 0xFF, color_int & 0xFF]
```

Fluorescence images should always be obtained in `tiff` format:

```python
# regions endpoint
region_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/0/start/0/0/size/1024/1024?image_format=tiff"
```

Therefore it is possible to specify all, but also only dedicated image channels. Dedicated channels can be selected with the optional parameter image_channels. By default all channels are selected. below selects only AF555- (Id 0) and DAPI-channel (Id 2) for the above given slide info:

```python
# regions endpoint
region_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/0/start/0/0/size/1024/1024?image_format=tiff&image_channels=0&image_channels=2
```


## WSIs with Z Coordinate

If the `z` property of the response of `endpoint_1` is greater than `1`, the `z` coordinate can be specified as query paramer:

```python
url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{size_x}/{size_y}?z=2"
url = f"{APP_API}/v0/{JOB_ID}/tiles/{wsi_id}/level/{level}/position/{pos_x}/{pos_y}?z=2"
```

Resp. in python:

```python
# regions endpoint
Image.open(BytesIO.content))
wsi_id = my_wsi["id"] # see endpoint_1 above
region_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/0/start/0/0/size/1024/1024"
params = {"z": 1}
r = requests.get(tile_url, headers=HEADERS, params=params)
img = Image.open(BytesIO(r.content))
```

!> At the current stage, this feature is not yet implemented.

## Supported WSI Formats

Currently the following WSI formats are supported:

- Hamamsatsu (`.ndpi`)
- 3DHistech (`.mrxs`)
- Aperio (`.svs`)
- Generic tiff (`.tif`/`.tiff`)
- OME-tif (`.ome.tif` at the moment only fluorescence)
- Philips (`.isyntax` currently only supported by the EMPAIA platform, but not the EMPAIA App Test Suite)
