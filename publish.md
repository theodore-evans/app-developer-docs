# Publish

To publish your app to the EMPAIA eco system you need to:
* Containerize your app.
* Upload your containerized app to the EMPAIA registry.
* Upload your EMPAIA-App-Desciption.

All sample apps in the [Sample Apps Repository (tutorial folder)](https://gitlab.com/empaia/integration/sample-apps/-/tree/master/sample_apps/valid/tutorial)
 contain all necessary files like the ones mentioned in this chapter.


## Containerized App Example

This section will show, how an application could be wrapped into a docker container.

This example describes how to containerize a Python app. For other programming languages, e.g. C++, Java or .Net, the project structure and Dockerfile have to be adapted accordingly. 

### Project structure

Assuming your applicaiton is written in python, you might have a `app.py`, with your apps main code, and some mechnism to specify needed packages and versions. For simplicity we chose a simple `requirements.txt` here. To containerize your app you need a `Dockerfile`. All three files are supposed to be in your apps root folder:

```
containerized_app_example/
  ├── app.py
  ├── requirements.txt
  └── Dockerfile
```


### app.py

For simplicity, and as you might already be familiar with that app, we take the [Tutorial Simple App](tutorial/simple_app.md#) as example.

### requirements.txt

```
Pillow==7.2.0
requests==2.24.0
```

### Dockerfile

```Dockerfile
FROM python:3.6
ADD . /
RUN pip install -r requirements.txt
ENTRYPOINT python3 -u /app.py
```


## Building, Testing and Publishing the App

### Building the Docker Image

For testing, you can just execute the `app.py` directly, but in the end, the app should run
as a Docker Image. To build the image, use the following, where `sample-app` is replaced with
the actual name of your app, preferably in the format `vendor/app:tag`:

    docker build -t sample-app .

Building the Image can take a moment as the requirements have to be installed each time. If your
requirements do not change much, you may consider creating a base image with just those requirements
and then building upon that image for a faster build process.

### Testing

For testing your please refer to [App Test Suite](app_test_suite).

### Publishing the App

Coming soon

* Login at EMPAIA Portal
* Fill out form to publish your app. This includes:
  * Name
  * Description
  * Media (e.g. images)
  * Upload docker imager
  * Upload EAD