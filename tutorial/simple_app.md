# Simple App

Suppose you have developed an algorithm which consumes a tile of a Whole-Slide-Image (WSI) and counts the number of tumorous cells. Two things are needed to create an EMPAIA App from your algorithm:

1. An EMPAIA App Description written in JSON (EAD)
2. Some glue code to receive/send input/output data from/to a RESTful HTTP inteface (API)

In this section we will:
1. Create an EAD for a simple app specifying:
  * Single WSI as input.
  * Single rectangle annotation as input.
  * Single integer output.
2. Write the code that is needed to talk to the API to receive the specified inputs and to send the specified output.

## Inputs

* 1 WSI (key: my_wsi).
* 1 Rectangle annotation (key: my_rectangle) specifying the region to be processed.

## Outputs

* 1 Tumor cell count (key: tumor_cell_count).

## EMPAIA-App-Description

```JSON
{
    "$schema": "https://developer.empaia.org/schema/ead-app-schema-draft-3.json",
    "name": "Tutorial App 01",
    "name_short": "TA01",
    "namespace": "org.empaia.vendor_name.tutorial_app_01.v1",
    "description": "Human readable description",
    "inputs": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangle": {
            "type": "rectangle",
            "reference": "inputs.my_wsi"
        }
    },
    "outputs": {
        "tumor_cell_count": {
            "type": "integer"
        }
    }
}
```

## App-Interface

Inside your app input and output parameters can be accessed via the App API. The endpoints are specified by the EAD:

```python
import os
from io import BytesIO

import requests
from PIL import Image

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}


def put_finalize():
    """
    finalize job, such that no more data can be added and to inform EMPAIA infrastructure about job state
    """
    url = f"{APP_API}/v0/{JOB_ID}/finalize"
    r = requests.put(url, headers=HEADERS)
    r.raise_for_status()


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v0/{JOB_ID}/inputs/{key}"
    r = requests.get(url, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD
    """
    url = f"{APP_API}/v0/{JOB_ID}/outputs/{key}"
    r = requests.post(url, json=data, headers=HEADERS)
    r.raise_for_status()
    return r.json()


def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    """
    get a WSI tile on level 0

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    x, y = my_rectangle["upper_left"]
    width = my_rectangle["width"]
    height = my_rectangle["height"]

    wsi_id = my_wsi["id"]
    level = 0

    tile_url = f"{APP_API}/v0/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{width}/{height}"
    r = requests.get(tile_url, headers=HEADERS)
    r.raise_for_status()

    return Image.open(BytesIO(r.content))


def count_tumor_cells(wsi_tile: Image):
    """
    pretends to do something useful

    Parameters:
        wsi_tile: WSI image tile
    """
    return 42


my_wsi = get_input("my_wsi")
my_rectangle = get_input("my_rectangle")

wsi_tile = get_wsi_tile(my_wsi, my_rectangle)

tumor_cell_count = {
    "name": "cell count tumor",  # choose name freely
    "type": "integer",
    "value": count_tumor_cells(wsi_tile),
}

post_output("tumor_cell_count", tumor_cell_count)

put_finalize()
```
