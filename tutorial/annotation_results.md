# Annotation Results

In order to count tumorous cells, the App needs to detect these cells. The EAD allows the app to upload the cell data as point annotations.

In this section we will:
* Add a collection of collections of annotations as output.

## Inputs

* 1 WSI (key: my_wsi).
* 1 Collection of many rectangle annotations (key: my_rectangles) specifying the regions to be processed.

## Outputs

* 1 Collection of collections of many tumor cell point annotations (key: tumor_cells). Each of the inner collections references on of the input rectangles from the my_rectangles collection.

## EMPAIA-App-Description

```JSON
{
    "$schema": "https://developer.empaia.org/schema/ead-app-schema-draft-3.json",
    "name": "Tutorial App 03",
    "name_short": "TA03",
    "namespace": "org.empaia.vendor_name.tutorial_app_03.v1",
    "description": "Human readable description",
    "inputs": {
        "my_wsi": {
            "type": "wsi"
        },
        "my_rectangles": {
            "type": "collection",
            "items": {
                "type": "rectangle",
                "reference": "inputs.my_wsi"
            }
        }
    },
    "outputs": {
        "my_cells": {
            "type": "collection",
            "items": {
                "type": "collection",
                "reference": "inputs.my_rectangles.items",
                "items": {
                    "type": "point",
                    "reference": "inputs.my_wsi"
                }
            }
        }
    }
}
```

* For each item in `my_rectangles` the app finds multiple cells, hence the `reference` from the inner collection to one rectangle `inputs.my_rectangles.items`.
* Annotations always reference whole slide images. Therefore each detected cell (point annotation) has a `reference` to `inputs.my_wsi`.
* For a list of possible reference combinations see [Specs - Data Types and References - References](specs/empaia_app_description.md#references).
* For a list of available annotation types see  [Specs - Data Types and References - Annotation Data Types](specs/empaia_app_description.md#annotation-data-types).


## App-Interface

```python
import os
import requests
from PIL import Image
from io import BytesIO

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
HEADERS = {"Authorization": f"Bearer {TOKEN}"}

def put_finalize():
    ... # see Simple App

def get_input(key: str):
    ... # see Simple App

def post_output(key: str, data: dict):
    ... # see Simple App

def get_wsi_tile(my_wsi: dict, my_rectangle: dict):
    ... # see Simple App

def detect_cells(my_wsi: dict, my_rectangle: dict):
    """
    pretends to do something useful

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    wsi_tile = get_wsi_tile(my_wsi, my_rectangle)
    _ = wsi_tile  # wsi_tile not used in dummy code below

    cells = {
        "item_type": "point",
        "items": [],
        "reference_id": my_rectangle["id"],  # point annotations collection references my_rectangle
        "reference_type": "annotation",
    }

    # your computational code below
    for _ in range(10):
        cell = {
            "name": "cell",
            "type": "point",
            "reference_id": my_wsi["id"],  # each point annotation references my_wsi
            "reference_type": "wsi",
            "coordinates": [250, 250],  # Always use WSI base level coordinates
            "npp_created": 499,  # pixel resolution level of the WSI, that has been used to create the annotation (relevant for viewer)
            "npp_viewing": [
                499,
                3992,
            ],  # (optional) recommended pixel reslution range for viewer to display annotation, if npp_created is not sufficient
        }
        cells["items"].append(cell)

    return cells


my_wsi = get_input("my_wsi")
my_rectangles = get_input("my_rectangles")

my_cells = {"item_type": "collection", "items": []}

for my_rectangle in my_rectangles["items"]:
    cells = detect_cells(my_wsi, my_rectangle)
    my_cells["items"].append(cells)

post_output("my_cells", my_cells)

put_finalize()
```

?> All annotation types have the properties `npp_created` and `npp_viewing` (abreviation for *nanometer per pixel*):

* `npp_created` is **required** and **must** contain the pixel resolution level of the WSI-tiles your app used to find the annotations, e.g. `499`.
* `npp_viewing` is **optional** and **should** contain a pixel resolution level range within it makes any sense for the annotations to be displayed when a pathologist views the WSI and the annotation, e.g. `[499, 3992]`:
  * `499` would be the base layer resolution if the WSI was scanned using 20x magnification. 
  * `3992` is the calculated resolution of the base layer multiplied by the downsample factor of level 3 (see below).
* These attributes are **only** taken into consideration for WSI-viewers, when it would be impossible to query and display all annotations at once, e.g. when millions of annotations reside on current viewport of the WSI.
* If the amount of total annotations within the current viewport of the WSI is not too much for the WSI-viewer, all annotations will be displayed regardless of `npp_created` and `npp_viewing`.
* The exact rules when to display which annotations and when to use other techniques to indicate existance of annotations (e.g. heatmaps, clustering, ...) depend on the specific implementation of the WSI-Viewer.
* By providing `npp_viewing` (optional) you have the oportunity to actively help viewers to decide what to show.
* The pixel resolution level can be calculated using the endpoint for the WSI, e.g.:
  * `base_layer_npp = get_input("my_wsi")["pixel_size_nm"]`
  * `level_3_downsample = get_input("my_wsi")["levels"][3]["downsample_factor"]`
  * `npp_level_3 = base_layer_npp * level_3_downsample`
