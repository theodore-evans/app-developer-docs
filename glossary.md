# Glossary

## AAA Service

The component for implementing all authentication, authorization and billing processes (accounting, including billing and licensing).
The AAA service is always provided centrally via the EMPAIA platform.

## Annotation

Annotations in digital histopathology on WSIs. Supported Annotatations are: `point`, `line`, `arrow`, `rectangle`, `polygon` and `circle`, see also [Specs - EMPAIA-App-Description - Annotation Data Types](specs/empaia_app_description.md#annotation-data-types).

## App

An app is a self-contained component for providing functionality and/or data processing via the EMPAIA architecture. Apps are the building blocks of a solution to enable more complex analysis / processing. In the context of a clinical application, apps should not be used directly, but always be part of a (certified) solution. Apps are divided into containerized apps and widgets.

**Note:** In the context of this document, version *Draft-1*, an app is always a Containerized App.

## App Service

The App Service impolements the App API. It is a provides an abstraction layer for the communication between a (containerized) app and the Medical Data API. All communication takes place via this interface. The App Service contacts the Medical Data API and transparently transfers the requested, app-specific identifiers to IDs of the Medical Data API.

**Note:** Future versions: The App Service validates if the request of the app (input and output) aligns with EAD. Requests not specified by the apps EAD will be rejected.

## Case

Pseudonymized medical data about a patients case. It includes WSIs and associated meta data.

## Container Registry

Docker registry storing published containerized apps.

## Containerized App

A Containerized App is an encapsulated algorithm / encapsulated AI model for execution in a Job Execution Service. Containerized Apps are usually complex, time-consuming and non-time-critical processing tasks that must be executed on a powerful infrastructure, e.g. a GPU cluster.

## EMPAIA App Description

The EMPAIA App Description (EAD) describes the inputs and outputs of an app with their corresponding data types. Data identifiers defined in the EAD are used to read and write data via the App API.

## Job

A job is a concrete execution of an app with concrete input and output parameters.

## Job Execution Service

The  Job Execution Service runs Containerized Apps in a compute cluster.

## Medical Data Service

The Medical Data Service implements the Medical Data API. The supported data types include WSIs, WSI meta data, Jobs and Job results (primitive data, annotations, collections, see [Specs - EMPAIA-App-Description - Data Types](specs/empaia_app_description.md#data-types)).

## PLIS

Pathology Laboratory Information System. Sometimes also Anatomic Pathology Laboratory Information System (AP-LIS).

## Region of Interest (RoI)

Interesting region of a WSI defined by pathologists. In the EMPAIA infrastructure RoIs are annotations of type `rectangle`, `polygon` or `circle` classified as `org.empaia.global.v1.classes.roi`.

## Solution Store Service

Stores EADs of apps and workflows descriptions of solutions.

## Vault Service

Global EMPAIA service to store key / values pairs for apps. E.g. for storing credintials used by an app to communicate with external APIs.

## Whole-Slide-Image (WSI)

Digitized slide.

## Workbench Client

The Workbench Client is the central user interface for diagnostics and research of the EMPAIA platform.

## Workbench Service

The Workbench Service implements the Workbench API. It allows the Workbench Client to connect to the EMPAIA infrastructure.
